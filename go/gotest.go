package main

import "fmt"

// findme function to search for an element in a slice
func findme(arr []int, target int) int {
	for i, v := range arr {
		if v == target {
			return i // Return the index if found
		}
	}
	return -1 // Return -1 if not found
}

func main() {
	// Example usage
	mySlice := []int{1, 2, 3, 4, 5}
	target := 3
	result := findme(mySlice, target)
	if result != -1 {
		fmt.Printf("Element found at index: %d\n", result)
	} else {
		fmt.Println("Element not found")
	}
}

